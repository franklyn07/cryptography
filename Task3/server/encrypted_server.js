const fs = require('fs')
const https = require('https');
const express = require('express')
const app = express()

app.get('/',(req,res)=>{
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.writeHead(200);
  res.end("<html><body><form action='/' method='post'>Username:<br><input type='text' name='username'><br>Password:<br><input type='text' name='password'><br><input type='submit'></form></body></html>");
})

app.post('/',(req,res)=>{
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.writeHead(200);
  res.end("<html><body><h1>Welcome</h1></body></html>");
})

const hostname = '127.0.0.1';
const port = 443;

const server = https.createServer({
  key: fs.readFileSync('www.helloworld.com.key.pem'),
  cert: fs.readFileSync('www.helloworld.com.cert.pem')
}, app);

server.listen(port, hostname, () => {
  console.log(`Server running at https://${hostname}:${port}/`);
});
