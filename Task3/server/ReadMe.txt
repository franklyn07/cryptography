--To Run Server--
$ sudo node http_server.js

--To access application--
firefox: https://www.helloworld.com 
http://www.helloworld.com

#Note that to accept certificate, the certificate instance was stored as an exception in firefox

#this was achieved by changing the /etc/hosts using nano and introducing the line 127.0.0.1 www.helloworld.com
