openssl req -config intermediate/openssl.cnf \
      -key intermediate/private/www.helloworld.com.key.pem \
      -new -sha256 -out intermediate/csr/www.helloworld.com.csr.pem

openssl genrsa -out intermediate/private/www.helloworld.com.key.pem 2048

chmod 400 intermediate/private/www.helloworld.com.key.pem

openssl req -config intermediate/openssl.cnf \
      -key intermediate/private/www.helloworld.com.key.pem \
      -new -sha256 -out intermediate/csr/www.helloworld.com.csr.pem

 openssl ca -config intermediate/openssl.cnf \
      -extensions server_cert -days 375 -notext -md sha256 \
      -in intermediate/csr/www.helloworld.com.csr.pem \
      -out intermediate/certs/www.helloworld.com.cert.pem

openssl verify -CAfile intermediate/certs/ca-chain.cert.pem \
      intermediate/certs/www.helloworld.com.cert.pem
